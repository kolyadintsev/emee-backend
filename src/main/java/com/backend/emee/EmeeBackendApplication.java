package com.backend.emee;

import com.backend.emee.services.MessageService;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootApplication
public class EmeeBackendApplication {

	public static void main(String[] args) {
        SpringApplication.run(EmeeBackendApplication.class, args);
        /*
        FirebaseOptions options = null;
        try {
            options = new FirebaseOptions.Builder()
                    .setServiceAccount(new FileInputStream("Emee-517180d7c2d0.json"))
                    .setDatabaseUrl("https://databaseName.firebaseio.com/")
                    .build();
            FirebaseApp.initializeApp(options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        MessageService message = MessageService.getInstance(); */
	}
}
