package com.backend.emee.messages;


import com.backend.emee.entity.User;

public class UserMessage {

    private long id;
    private String login;
    private String firstName;
    private String lastName;
    private String city;
    private byte age;
    private boolean sex; //true - male, false - female
    private String url;
    private String bio;
    private String photoUrl;
    private boolean isInSearch;
    private boolean isPrivate;
    private long followers;
    private long followings;
    private long events;
    private boolean canFollow;

    public UserMessage(User user, boolean canFollow, long events) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.city = user.getCity();
        this.age = user.getAge();
        this.sex = user.isSex();
        this.url = user.getUrl();
        this.bio = user.getBio();
        this.photoUrl = user.getPhotoUrl();
        this.isInSearch = user.getIsInSearch();
        this.isPrivate = user.getIsPrivate();
        this.followers = user.getFollowers().size();
        this.followings = user.getFollowing().size();
        this.canFollow = canFollow;
        this.events = events;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public byte getAge() {
        return age;
    }

    public void setAge(byte age) {
        this.age = age;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean isInSearch() {
        return isInSearch;
    }

    public void setInSearch(boolean inSearch) {
        isInSearch = inSearch;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public long getFollowers() {
        return followers;
    }

    public void setFollowers(long followers) {
        this.followers = followers;
    }

    public long getFollowings() {
        return followings;
    }

    public void setFollowings(long followings) {
        this.followings = followings;
    }

    public long getEvents() {
        return events;
    }

    public void setEvents(long events) {
        this.events = events;
    }

    public boolean isCanFollow() {
        return canFollow;
    }

    public void setCanFollow(boolean canFollow) {
        this.canFollow = canFollow;
    }
}
