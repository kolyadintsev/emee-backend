package com.backend.emee.messages;


import com.backend.emee.entity.Message;
import com.backend.emee.entity.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConversationMessage {

    private Long id;
    private String text;
    private String date;
    private Long creator_id;
    private Long chat_id;

    public ConversationMessage() {
    }

    public ConversationMessage(Long id, String text, String date, Long creator_id, Long chat_id) {
        this.id = id;
        this.text = text;
        this.date = date;
        this.creator_id = creator_id;
        this.chat_id = chat_id;
    }

    public ConversationMessage (Message message) {
        this.id = message.getId();
        this.text = message.getText();
        this.date = message.getDate().toString();
        this.creator_id = message.getCreator().getId();
        this.chat_id = message.getChat().getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(Long creator_id) {
        this.creator_id = creator_id;
    }

    public Long getChat_id() {
        return chat_id;
    }

    public void setChat_id(Long chat_id) {
        this.chat_id = chat_id;
    }
}
