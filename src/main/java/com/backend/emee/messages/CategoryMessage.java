package com.backend.emee.messages;

import com.backend.emee.entity.EventCategory;

/**
 * Created by Michael on 19.06.2016.
 */
public class CategoryMessage {
    private long id;
    private String name;

    public CategoryMessage() {
    }

    public CategoryMessage(EventCategory category) {
        this.id = category.getId();
        this.name = category.getName();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
