package com.backend.emee.messages;

import com.backend.emee.entity.Event;
import com.backend.emee.entity.EventCategory;

import java.util.Date;
import java.util.Set;

/**
 * Created by Michael on 18.06.2016.
 */
public class EventMessage {
    private long id;
    private Long creator_id;
    private String name;
    private Date startDate;
    private Date endDate;
    private String price;
    private String ageRestrictions;
    private String photoUrl;
    private String description;
    private Set<EventCategory> category;
    private String address;
    private long going;
    private long notSure;
    private long likes;

    public EventMessage() {
    }

    public EventMessage(Event event) {
        this.id = event.getId();
        this.creator_id = event.getCreator().getId();
        this.name = event.getName();
        this.startDate = event.getStartDate();
        this.endDate = event.getEndDate();
        this.price = event.getPrice();
        this.ageRestrictions = event.getAgeRestrictions();
        this.photoUrl = event.getPhotoUrl();
        this.description = event.getDescription();
        this.category = event.getCategory();
        this.address = event.getAddress();
        this.going = event.getGoing().size();
        this.notSure = event.getNotSure().size();
        this.likes = event.getLikes().size();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(Long creator_id) {
        this.creator_id = creator_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAgeRestrictions() {
        return ageRestrictions;
    }

    public void setAgeRestrictions(String ageRestrictions) {
        this.ageRestrictions = ageRestrictions;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<EventCategory> getCategory() {
        return category;
    }

    public void setCategory(Set<EventCategory> category) {
        this.category = category;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getGoing() {
        return going;
    }

    public void setGoing(long going) {
        this.going = going;
    }

    public long getNotSure() {
        return notSure;
    }

    public void setNotSure(long notSure) {
        this.notSure = notSure;
    }

    public long getLikes() {
        return likes;
    }

    public void setLikes(long likes) {
        this.likes = likes;
    }
}
