package com.backend.emee.controllers;

import com.backend.emee.entity.Event;
import com.backend.emee.messages.EventMessage;
import com.backend.emee.services.EventService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Michael on 18.06.2016.
 */
@Controller
@RequestMapping("/events")
public class EventController {

    @Resource
    private EventService eventService;

    @RequestMapping(value = "/findByUser", method= RequestMethod.GET)
    public @ResponseBody
    List<EventMessage> findByUser(@RequestParam(value="id", defaultValue="0") String id) {
        return eventService.findByUser(Long.parseLong(id));
    }

    @RequestMapping(value = "/findByName", method= RequestMethod.GET)
    public @ResponseBody
    List<EventMessage> findByName(@RequestParam(value="name", defaultValue="") String name) {
        return eventService.findByName(name);
    }

    @RequestMapping(value = "/findAll", method= RequestMethod.GET)
    public @ResponseBody
    List<EventMessage> findAll() {
        return eventService.findAll();
    }

    @RequestMapping(value = "/findById", method= RequestMethod.GET)
    public @ResponseBody
    EventMessage findById(@RequestParam(value="id", defaultValue="0") String id) {
        return eventService.findById(Long.parseLong(id));
    }

    @RequestMapping(value = "/createEvent", method= RequestMethod.POST)
    public boolean createEvent(@RequestBody EventMessage event) {
        return eventService.createEvent(event);
    }

}
