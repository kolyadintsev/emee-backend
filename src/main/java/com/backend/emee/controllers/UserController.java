package com.backend.emee.controllers;


import com.backend.emee.entity.User;
import com.backend.emee.messages.ChatMessage;
import com.backend.emee.messages.FeedMessage;
import com.backend.emee.messages.PasswordMessage;
import com.backend.emee.messages.UserMessage;
import com.backend.emee.services.ChatService;
import com.backend.emee.services.UserService;
import org.apache.commons.logging.Log;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@Controller
@RequestMapping("/users")
public class UserController {

    @Resource
    private UserService userService;


    @RequestMapping(value = "/findByName", method= RequestMethod.GET)
    public @ResponseBody
    List<UserMessage> findByName(@RequestParam(value="name", defaultValue="") String name,
                                 @RequestParam(value="requested_id", defaultValue="0") String request_id) {
        return userService.findByName(name, request_id);
    }

    @RequestMapping(value = "/findAll", method= RequestMethod.GET)
    public @ResponseBody
    List<UserMessage> findAll (@RequestParam(value="requested_id", defaultValue="0") String request_id) {
        return userService.findAll(request_id);
    }

    @RequestMapping(value = "/findFeed", method= RequestMethod.GET)
    public @ResponseBody
    List<FeedMessage> findFeed (@RequestParam(value="requested_id", defaultValue="0") String request_id) {
        return userService.findFeed(request_id);
    }

    @RequestMapping(value = "/findByLogin", method= RequestMethod.GET)
    public @ResponseBody
    UserMessage findByLogin(@RequestParam(value="login", defaultValue="") String login,
                            @RequestParam(value="requested_id", defaultValue="0") String request_id) {
        return userService.findByLogin(login, request_id);
    }

    @RequestMapping(value = "/findById", method= RequestMethod.GET)
    public @ResponseBody
    UserMessage findById(@RequestParam(value="id", defaultValue = "0") String id,
                         @RequestParam(value="requested_id", defaultValue="0") String request_id) {
        return userService.findById(id, request_id);
    }

    @RequestMapping(value = "/findChats", method= RequestMethod.GET)
    public @ResponseBody
    List<ChatMessage> findById(@RequestParam(value="requested_id", defaultValue="0") String request_id) {
        return userService.findChats(request_id);
    }

    @RequestMapping(value = "/createUser", method= RequestMethod.POST)
    public boolean createUser(@RequestBody UserMessage userMessage, String password) {
        return userService.createUser(new User(userMessage, password));
    }

    @RequestMapping(value = "/changePassword", method= RequestMethod.POST)
    public boolean changePassword(@RequestBody PasswordMessage passwordMessage){
        return userService.changePassword(passwordMessage);
    }

}
