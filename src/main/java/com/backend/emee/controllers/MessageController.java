package com.backend.emee.controllers;

import com.backend.emee.messages.ChatMessage;
import com.backend.emee.messages.ConversationMessage;
import com.backend.emee.services.ChatService;
import com.backend.emee.services.MessageService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/message")
public class MessageController {
/*
    @RequestMapping(method= RequestMethod.GET)
    public @ResponseBody
    ConversationMessage getMessage (@RequestParam(value="device_id", defaultValue="") String id) {
        return MessageService.getInstance();
    }

    @RequestMapping(method= RequestMethod.POST)
    public @ResponseBody
    Boolean sendMessage (@RequestParam(value="device_id", defaultValue="") String id) {
        MessageService.getInstance();
        return true;
    }
    */

    @Resource
    private ChatService chatService;

    @RequestMapping(value = "/findById", method= RequestMethod.GET)
    public @ResponseBody
    ChatMessage findById(@RequestParam(value="requested_id", defaultValue="0") String request_id) {
        return chatService.findById(request_id);
    }

    @RequestMapping(value = "/findMessageById", method= RequestMethod.GET)
    public @ResponseBody
    ConversationMessage findMessageById (@RequestParam(value="requested_id", defaultValue="0") String request_id) {
        return chatService.findMessageById(request_id);
    }

    @RequestMapping(value = "/findMessagesByChat", method= RequestMethod.GET)
    public @ResponseBody
    List<ConversationMessage> findMessagesByChat (@RequestParam(value="requested_id", defaultValue="0") String request_id) {
        return chatService.findMessagesByChat(request_id);
    }

    @RequestMapping(value = "/createChat", method= RequestMethod.POST)
    public long createChat(@RequestBody ChatMessage chatMessage) {
        return chatService.createChat(chatMessage);
    }

    @RequestMapping(value = "/createMessage", method= RequestMethod.POST)
    public boolean createMessage(@RequestBody ConversationMessage conversationMessage) {
        return chatService.createMessage(conversationMessage);
    }
}
