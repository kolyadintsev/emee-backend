package com.backend.emee.controllers;

import com.backend.emee.messages.LoginMessage;
import com.backend.emee.messages.UserMessage;
import com.backend.emee.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Resource
    private UserService userService;

    @RequestMapping(method= RequestMethod.GET)
    public @ResponseBody
    LoginMessage login (@RequestParam(value="login", defaultValue="") String login,
                        @RequestParam(value="password", defaultValue="") String password) {
        return userService.Login(login, password);
    }

}
