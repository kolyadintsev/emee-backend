package com.backend.emee.entity;

import com.backend.emee.messages.UserMessage;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Michael on 06.02.2016.
 */
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private long id;

    @Column (name = "login", nullable = false, length = 15, unique = true)
    private String login;

    @Column (name = "password", nullable = false, length = 20)
    private String password;

    @Column (name = "firstName", nullable = false)
    private String firstName;

    @Column (name = "lastName", nullable = false)
    private String lastName;

    @Column (name = "city", nullable = false)
    private String city;

    @Column (name = "age", nullable = false)
    private byte age;

    @Column (name = "sex", nullable = false)
    private boolean sex;
    //true - male, false - female

    @Column (name = "url")
    private String url;

    @Column (name = "bio")
    private String bio;

    @Column (name = "photo")
    private String photoUrl;

    @Column (name = "search", nullable = false)
    private boolean isInSearch;

    @Column (name = "private", nullable = false)
    private boolean isPrivate;

    @ManyToMany
    @JoinTable(name="following_follower",
            joinColumns = @JoinColumn(name="follower_id", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name="following_id", referencedColumnName="id"))
    private Set<User> following;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "following")
    private Set<User> followers;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "id")
    private Set<Event> events;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "going")
    private Set<Event> goingEvents;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "notSure")
    private Set<Event> notSureEvents;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "likes")
    private Set<Event> likesEvents;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "id")
    private Set<Comment> comments;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "users")
    private Set<Chat> chats;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "id")
    private Set<Message> messages;

    public User() {
    }

    public User(String login, String password, String firstName, String lastName, String city, byte age, boolean sex, String url, String bio, String photoUrl, boolean isInSearch, boolean isPrivate, Set<User> following, Set<User> followers, Set<Event> events, Set<Event> goingEvents, Set<Event> notSureEvents, Set<Event> likesEvents, Set<Comment> comments, Set<Chat> chats, Set<Message> messages) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.age = age;
        this.sex = sex;
        this.url = url;
        this.bio = bio;
        this.photoUrl = photoUrl;
        this.isInSearch = isInSearch;
        this.isPrivate = isPrivate;
        this.following = following;
        this.followers = followers;
        this.events = events;
        this.goingEvents = goingEvents;
        this.notSureEvents = notSureEvents;
        this.likesEvents = likesEvents;
        this.comments = comments;
        this.chats = chats;
        this.messages = messages;
    }

    public User(UserMessage userMessage, String password) {
        this.login = userMessage.getLogin();
        if (!password.equals("")) {
            this.password = password;
        }
        this.firstName = userMessage.getFirstName();
        this.lastName = userMessage.getLastName();
        this.city = userMessage.getCity();
        this.age = userMessage.getAge();
        this.sex = userMessage.isSex();
        this.url = userMessage.getUrl();
        this.bio = userMessage.getBio();
        this.photoUrl = userMessage.getPhotoUrl();
        this.isInSearch = userMessage.isInSearch();
        this.isPrivate = userMessage.isPrivate();
        this.following = null;
        this.followers = null;
        this.events = null;
        this.goingEvents = null;
        this.notSureEvents = null;
        this.likesEvents = null;
        this.comments = null;
        this.messages = null;
        this.chats = null;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isInSearch() {
        return isInSearch;
    }

    public void setInSearch(boolean inSearch) {
        isInSearch = inSearch;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public Set<Event> getGoingEvents() {
        return goingEvents;
    }

    public void setGoingEvents(Set<Event> goingEvents) {
        this.goingEvents = goingEvents;
    }

    public Set<Event> getNotSureEvents() {
        return notSureEvents;
    }

    public void setNotSureEvents(Set<Event> notSureEvents) {
        this.notSureEvents = notSureEvents;
    }

    public Set<Event> getLikesEvents() {
        return likesEvents;
    }

    public void setLikesEvents(Set<Event> likesEvents) {
        this.likesEvents = likesEvents;
    }

    public long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public byte getAge() {
        return age;
    }

    public void setAge(byte age) {
        this.age = age;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean getIsInSearch() {
        return isInSearch;
    }

    public void setIsInSearch(boolean isInSearch) {
        this.isInSearch = isInSearch;
    }

    public boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public Set<User> getFollowing() {
        return following;
    }

    public void setFollowing(Set<User> following) {
        this.following = following;
    }

    public Set<User> getFollowers() {
        return followers;
    }

    public void setFollowers(Set<User> followers) {
        this.followers = followers;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }

    public Set<Chat> getChats() {
        return chats;
    }

    public void setChats(Set<Chat> chats) {
        this.chats = chats;
    }

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }
}
