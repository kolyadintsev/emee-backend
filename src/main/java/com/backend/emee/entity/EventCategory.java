package com.backend.emee.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "category")
public class EventCategory {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private byte id;

    private String name;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "category")
    private Set<Event> events;

    public EventCategory() {
    }

    public EventCategory(String name, Set<Event> events) {
        this.name = name;
        this.events = events;
    }

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }
}
