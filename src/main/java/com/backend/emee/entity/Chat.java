package com.backend.emee.entity;


import com.backend.emee.messages.ChatMessage;
import com.backend.emee.repositories.UserRepository;
import com.backend.emee.services.UserService;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.Set;
import java.util.TreeSet;

@Entity
public class Chat {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private long id;

    @ManyToMany
    @JoinTable(name="chat_users",
            joinColumns = @JoinColumn(name="chat_id", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name="user_id", referencedColumnName="id"))
    private Set<User> users;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "id")
    private Set<Message> messages;

    public Chat(Set<User> users, Set<Message> messages) {
        this.users = users;
        this.messages = messages;
    }

    public Chat() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }

}
