package com.backend.emee.repositories;

import com.backend.emee.entity.Event;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Michael on 18.06.2016.
 */
public interface EventRepository  extends CrudRepository<Event, Long> {
    List<Event> findByCreator_id(@Param("creator_id") long creator_id);
    Event findById (@Param("id") long id);
    Iterable<Event> findAll(Sort sort);
    List<Event> findByName(@Param("name") String name);

}
