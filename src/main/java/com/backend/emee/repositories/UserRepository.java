package com.backend.emee.repositories;

import com.backend.emee.entity.User;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {

    List<User> findBylastName(@Param("lastName") String name);
    User findBylogin (@Param("login") String login);
    User findByid (@Param("id") long id);

}
