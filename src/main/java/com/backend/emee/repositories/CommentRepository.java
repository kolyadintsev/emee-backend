package com.backend.emee.repositories;

import com.backend.emee.entity.Comment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Michael on 18.06.2016.
 */
public interface CommentRepository extends CrudRepository<Comment, Long>{
    List<Comment> findByCreator_id(@Param("creator_id") long creator_id);
    List<Comment> findByEvent_id(@Param("event_id") long event_id);
    Comment findById (@Param("id") long id);

}
