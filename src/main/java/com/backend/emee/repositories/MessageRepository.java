package com.backend.emee.repositories;


import com.backend.emee.entity.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface MessageRepository extends CrudRepository<Message, Long> {
    Message findById (@Param("id") long id);

}
