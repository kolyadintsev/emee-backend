package com.backend.emee.repositories;

import com.backend.emee.entity.EventCategory;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Michael on 18.06.2016.
 */
public interface EventCategoryRepository extends CrudRepository<EventCategory, Long> {


}
