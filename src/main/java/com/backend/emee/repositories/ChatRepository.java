package com.backend.emee.repositories;


import com.backend.emee.entity.Chat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ChatRepository extends CrudRepository<Chat, Long>{
    Chat findById (@Param("id") long id);

}
