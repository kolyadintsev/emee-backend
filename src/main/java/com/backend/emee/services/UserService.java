package com.backend.emee.services;

import com.backend.emee.entity.Chat;
import com.backend.emee.entity.Event;
import com.backend.emee.entity.Message;
import com.backend.emee.entity.User;
import com.backend.emee.messages.*;
import com.backend.emee.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


@Service
@Transactional
public class UserService {
    @Autowired
    private UserRepository repository;

    public List<UserMessage> findByName (String name, String request_id){
        List<UserMessage> messageList = new ArrayList<>();
        User request_user = repository.findByid(Long.parseLong(request_id));
        for (User user : repository.findBylastName(name)){
            boolean canFollow = false;
            if (request_user.getId() != 0) {
                canFollow = user.getFollowers().contains(request_user);
            }
            long events = user.getEvents().size();
            messageList.add(new UserMessage(user, canFollow, events));
        }
        return messageList;
    }

    public UserMessage findByLogin (String login, String request_id) {
        User request_user = repository.findByid(Long.parseLong(request_id));
        User user = repository.findBylogin(login);
        boolean canFollow = false;
        if (request_user.getId() != 0) {
            canFollow = user.getFollowers().contains(request_user);
        }
        long events = user.getEvents().size();
        return new UserMessage(user, canFollow, events);
    }

    public UserMessage findById (String id, String request_id) {
        boolean canFollow = false;
        Long user_id = Long.parseLong(id);
        Long req_id = Long.parseLong(request_id);
        User user = repository.findByid(user_id);
        if (req_id != 0) {
            User request_user = repository.findByid(req_id);
            canFollow = user.getFollowers().contains(request_user);
        }

        long events = user.getEvents().size();
        return new UserMessage(user, canFollow, events);
    }

    public LoginMessage Login (String login, String password) {
        User user = repository.findBylogin(login);
        long id = 0;
        boolean result = false;
        if (user.getPassword().equals(password)) {
            result = true;
            id = user.getId();
        }
        return new LoginMessage(result, id);
    }

    public boolean createUser (User newUser){
        try {
            User oldUser = repository.findByid(newUser.getId());
            if (oldUser != null) {
                newUser.setComments(oldUser.getComments());
                newUser.setEvents(oldUser.getEvents());
                newUser.setFollowers(oldUser.getFollowers());
                newUser.setFollowing(oldUser.getFollowing());
                newUser.setGoingEvents(oldUser.getGoingEvents());
                newUser.setLikesEvents(oldUser.getLikesEvents());
                newUser.setNotSureEvents(oldUser.getNotSureEvents());
                if (newUser.getPassword().equals("")){
                    newUser.setPassword(oldUser.getPassword());
                }
            }
            repository.save(newUser);
            return true;
        } catch (Exception ex){
            return false;
        }
    }

    public boolean changePassword (PasswordMessage passwordMessage) {
        try {
            User user = repository.findByid(passwordMessage.getUser_id());
            if (user.getPassword().equals(passwordMessage.getOldpassword())){
                user.setPassword(passwordMessage.getNewpassword());
                repository.save(user);
                return true;
            } else {
                return false;
            }
        } catch (Exception e){
            return false;
        }
    }

    public List<UserMessage> findAll (String request_id){
        List<UserMessage> messageList = new ArrayList<>();
        User request_user = repository.findByid(Long.parseLong(request_id));
        for (User user : repository.findAll()){
            boolean canFollow = false;
            if (user.isInSearch()) {
                if (request_user.getId() != 0) {
                canFollow = user.getFollowers().contains(request_user);
                }
                long events = user.getEvents().size();
                messageList.add(new UserMessage(user, canFollow, events));
            }
        }
        return messageList;
    }

    public List<FeedMessage> findFeed (String request_id){
        List<FeedMessage> messageList = new ArrayList<>();
        List<User> usersList = new ArrayList<>();
        User request_user = repository.findByid(Long.parseLong(request_id));
        for (User user : repository.findAll()){
            if ((request_user.getId() != 0) && user.getFollowers().contains(request_user)) {
                usersList.add(user);
            }
        }
        for (User user : usersList){
            for (Event event : user.getEvents()) {
                long events = user.getEvents().size();
                messageList.add(new FeedMessage(new UserMessage(user, true, events), new EventMessage(event), event.getStartDate()));
            }
        }
        messageList.sort(new Comparator<FeedMessage>() {
            @Override
            public int compare(FeedMessage o1, FeedMessage o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        });
        return messageList;
    }

    public List<ChatMessage> findChats (String request_id) {
        List<ChatMessage> chats = new ArrayList<>();
        User request_user = repository.findByid(Long.parseLong(request_id));
        for (Chat chat : request_user.getChats()) {
            chats.add(new ChatMessage(chat));
        }
        chats.sort(new Comparator<ChatMessage>() {
            @Override
            public int compare(ChatMessage o1, ChatMessage o2) {
                return o1.getMessages().get(o1.getMessages().size() - 1).compareTo(o2.getMessages().get(o2.getMessages().size() - 1));
            }
        });
        Collections.reverse(chats);
        return chats;
    }

    public List<ConversationMessage> findMessages (String request_id) {
        List<ConversationMessage> messageList = new ArrayList<>();
        User request_user = repository.findByid(Long.parseLong(request_id));
        for (Message message : request_user.getMessages()) {
            messageList.add(new ConversationMessage(message));
        }
        messageList.sort(new Comparator<ConversationMessage>() {
            @Override
            public int compare(ConversationMessage o1, ConversationMessage o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        Collections.reverse(messageList);
        return messageList;
    }


}
