package com.backend.emee.services;

import com.backend.emee.entity.Comment;
import com.backend.emee.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Michael on 18.06.2016.
 */
@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;

    public List<Comment> findByUser (long id) {
        return commentRepository.findByCreator_id(id);
    }

    public List<Comment> findByEvent (long id) {
        return commentRepository.findByEvent_id(id);
    }

    public Comment findById (long id) {
        return commentRepository.findById(id);
    }

    public boolean createComment (Comment comment) {
        try {
            commentRepository.save(comment);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
