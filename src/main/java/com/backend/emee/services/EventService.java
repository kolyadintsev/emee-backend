package com.backend.emee.services;

import com.backend.emee.entity.Event;
import com.backend.emee.entity.User;
import com.backend.emee.messages.EventMessage;
import com.backend.emee.repositories.EventRepository;
import com.backend.emee.repositories.UserRepository;
import jdk.nashorn.internal.runtime.ECMAException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michael on 18.06.2016.
 */
@Service
@Transactional
public class EventService {
    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserRepository userRepository;

    public List<EventMessage> findByUser (long id){
        List<EventMessage> messageList = new ArrayList<>();
        for (Event event : eventRepository.findByCreator_id(id)){
            messageList.add(new EventMessage(event));
        }
        return messageList;
    }

    public EventMessage findById (long id) {
        Event event = eventRepository.findById(id);
        return new EventMessage(event);
    }

    public boolean createEvent (EventMessage event) {
        try {
            eventRepository.save(new Event(event));
            return  true;
        } catch (Exception ex){
            return false;
        }
    }

    public User addEventCreator(EventMessage eventMessage) {
        try {
            User user = userRepository.findByid(eventMessage.getCreator_id());
            return user;
        } catch (Exception ex) {
            return new User();
        }
    }

    public List<EventMessage> findAll (){
        List<EventMessage> messageList = new ArrayList<>();
        Sort sort = new Sort(Sort.Direction.ASC, "startDate");
        Iterable<Event> events = eventRepository.findAll(sort);
        for (Event event : events){
            messageList.add(new EventMessage(event));
        }
        return messageList;
    }

    public List<EventMessage> findByName (String name){
        List<EventMessage> messageList = new ArrayList<>();
        for (Event event : eventRepository.findByName(name)){
            messageList.add(new EventMessage(event));
        }
        return messageList;
    }
}
