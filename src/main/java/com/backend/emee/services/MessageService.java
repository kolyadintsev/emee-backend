package com.backend.emee.services;


import org.jivesoftware.smack.*;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;

import javax.net.ssl.SSLSocketFactory;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.IOException;
import java.sql.Connection;

/**
 * Created by Michael on 26.07.2016.
 */
public class MessageService {
    private String HOST = "fcm-xmpp.googleapis.com";
    private int PORT = 5235;

    private static MessageService Instance;

    public static MessageService getInstance() {
        if (Instance == null) {
            Instance = new MessageService();
        }
        return Instance;
    }

    public void setInstance(MessageService instance) {
        Instance = instance;
    }

    public MessageService() {
// Create the configuration for this new connection
        ConnectionConfiguration config = new ConnectionConfiguration(HOST, PORT);
        config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        config.setReconnectionAllowed(true);
        config.setRosterLoadedAtLogin(false);
        config.setSendPresence(false);
        config.setSocketFactory(SSLSocketFactory.getDefault());
        config.setDebuggerEnabled(false);
        XMPPTCPConnection connection = new XMPPTCPConnection(config);


        try {
            connection.connect();
        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Log into the server
        try {
            connection.login("721092810940@gcm.googleapis.com", "AIzaSyDwUMPPA571Ujw1vA5wU1T_fK5TEN9w-dc");
        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Disconnect from the server
        //connection.disconnect();
    }


}

