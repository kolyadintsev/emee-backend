package com.backend.emee.services;

import com.backend.emee.entity.Chat;
import com.backend.emee.entity.Message;
import com.backend.emee.entity.User;
import com.backend.emee.messages.ChatMessage;
import com.backend.emee.messages.ConversationMessage;
import com.backend.emee.messages.UserMessage;
import com.backend.emee.repositories.ChatRepository;
import com.backend.emee.repositories.MessageRepository;
import com.backend.emee.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class ChatService {

    @Autowired
    private ChatRepository repository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    UserRepository userRepository;

    public ChatMessage findById (String id) {
        Long chat_id = Long.parseLong(id);
        Chat chat = repository.findById(chat_id);
        return new ChatMessage(chat);
    }

    public ConversationMessage findMessageById (String id){
        Long message_id = Long.parseLong(id);
        Message message = messageRepository.findById(message_id);
        return new ConversationMessage(message);
    }

    public List<ConversationMessage> findMessagesByChat (String id) {
        Long chat_id = Long.parseLong(id);
        Chat chat = repository.findById(chat_id);
        List<ConversationMessage> messageList = new ArrayList<>();
        for (Message message : chat.getMessages()){
            messageList.add(new ConversationMessage(message));
        }
        messageList.sort(new Comparator<ConversationMessage>() {
            @Override
            public int compare(ConversationMessage o1, ConversationMessage o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        return messageList;
    }

    public long createChat (ChatMessage chatMessage){
        long result = 0;
        try {
            Chat chat = repository.save(ChatFromMessage(chatMessage));
            result = chat.getId();
        } catch (Exception ex){
            result = 0;
        }
        return result;
    }

    public boolean createMessage (ConversationMessage conversationMessage){
        boolean result = false;
        try {
            messageRepository.save(MessageFromConversationMessage(conversationMessage));
            result = true;
        } catch (Exception ex){
            result = false;
        }
        return result;
    }

    private Chat ChatFromMessage (ChatMessage chatMessage){
            Set<User> users = new TreeSet<>();
            for (Long id: chatMessage.getUsers()) {
                users.add(userRepository.findByid(id));
            }
        Set<Message> messages = new TreeSet<>();
        for (Long id: chatMessage.getMessages()) {
            messages.add(messageRepository.findById(id));
        }
        return new Chat(users, messages);
    }

    private Message MessageFromConversationMessage (ConversationMessage message){
            User creator = userRepository.findByid(message.getId());
            Chat chat = repository.findById(message.getChat_id());
            String text = message.getText();
            DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh/mm/ss", Locale.ENGLISH);
        Date date = new Date();
            try {
                date = format.parse(message.getDate());
            } catch (ParseException e) {
                date = new Date();
            }
        return new Message(creator, chat, text, date);
    }

}
